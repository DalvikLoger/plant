import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
import tensorflow
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D,GlobalAveragePooling2D,Dropout, Flatten, Dense
from tensorflow.keras import regularizers
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import LabelEncoder # one-hot encoding for age
from keras.utils import to_categorical
from PIL import Image
from sklearn.model_selection import train_test_split
import cv2
import os
import sys
sns.set_theme()
 
from keras.utils import to_categorical
from sklearn import metrics
from matplotlib import cm
import itertools
from tensorflow.keras.datasets.mnist import load_data
from tensorflow.keras import datasets, layers, models

df = pd.read_csv('plant.csv', sep=';')
df = df.drop('Unnamed: 0',axis=1)

dm = pd.DataFrame(columns=('id','type'))
dm['type'] = 'Black-grass', 'Charlock', 'Cleavers', 'Common Chickweed', 'Common wheat', 'Fat Hen', 'Loose Silky-bent', 'Maize', 'nonsegmented','Scentless Mayweed', 'Shepherds Purse', 'Small-flowered Cranesbill', 'Sugar beet'
dm['value'] = 334,454,348,716,258,543,805,260,454,608,276,580,464

plt.figure(figsize=(18,6))
plt.bar(dm.type,dm.value)
plt.xticks(rotation=30)
plt.show()