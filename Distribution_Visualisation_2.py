import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
import tensorflow
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D,GlobalAveragePooling2D,Dropout, Flatten, Dense
from tensorflow.keras import regularizers
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import LabelEncoder # one-hot encoding for age
from keras.utils import to_categorical
from PIL import Image
from sklearn.model_selection import train_test_split
import cv2
import os
os.sys.path
import sys
sns.set_theme()
from pathlib import Path
 
from keras.utils import to_categorical
from sklearn import metrics
from matplotlib import cm
import itertools
from tensorflow.keras.datasets.mnist import load_data
from tensorflow.keras import datasets, layers, models

dm['name'] = 'Apple','Cherry', 'Corn', 'Grape', 'Orange', 'Peach', 'Pepper', 'Potato','Raspberry', 'Soybean', 'Squash', 'Strawberry','Tomato'
dm['disease'] = 3, 1, 3, 3, 1, 1, 1, 2, 0, 0, 1, 1, 9

plt.figure(figsize=(18,6))
plt.bar(dm.name, dm.disease)
plt.show()


